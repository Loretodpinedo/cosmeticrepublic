const LocalStrategy = require("passport-local").Strategy;
const User = require("../models/User.model");
const bcrypt = require("bcrypt");

const loginStrategy = new LocalStrategy(
    {
        usernameField: "email",
        passwordField: "password",
        passReqToCallback: true,
    },

    async (req, email, password, done) => {
        try {
            const existinUser = await User.findOne({ email });

            if (!existinUser) {
                const error = new Error("User is incorrect or does not exist");
                error.status = 401;
                return done(error);
            }

            const isValidPassport = await bcrypt.compare(
                password,
                existinUser.password
            );

            if (!isValidPassport) {
                const error = new Error("password is invalid");
                return done(error);
            }

            return done(null, existinUser);
        } catch (error) {
            return done(error);
        }
    }
);

module.exports = loginStrategy;
