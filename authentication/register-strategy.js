const LocalStrategy = require("passport-local").Strategy;
const User = require("../models/User.model");
const bcrypt = require("bcrypt");

const saltRounds = 10;

const validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
};

const validatePass = (password) => {
    const re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
    return re.test(String(password));
};

const registerStrategy = new LocalStrategy(
    {
        usernameField: "email",
        passwordField: "password",
        passReqToCallback: true,
    },

    async (req, email, password, done) => {
        try {
            const existingUser = await User.findOne({ email: email });

            if (existingUser) {
                const error = new Error("The user is already register");
                return done(error);
            }

            const isValidEMail = validateEmail(email);

            if (!isValidEMail) {
                const error = new Error("This email is invalid, do not cheat.");
                return done(error);
            }

            const isValidPassword = validatePass(password);

            if (!isValidPassword) {
                const error = new Error(
                    "The password must contain 6 to 20 characters, an uppercase, a lowercase and a number"
                );
                error.status = 400;
                return done(error);
            }

            const hash = await bcrypt.hash(password, saltRounds);

            const newUser = new User({
                name: req.body.name,
                email: email,
                password: hash,
                role: "user",
            });

            const savedUser = await newUser.save();

            return done(null, savedUser);
        } catch (error) {
            return done(error);
        }
    }
);

module.exports = registerStrategy;
