const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const productsSchema = new Schema(
    {
        type: {
            type: String,
            required: true,
            enum: [
                "Jabón",
                "Crema",
                "Gel de baño",
                "Gel hidratante",
                "Bomba de baño",
                "Champú",
                "Mascarilla pelo",
                "Mascarilla facial",
                "Dentifrico",
                "Manteca corporal",
            ],
        },
        name: { type: String, required: true },
        solid: { type: Boolean },
        ingredients: [{ type: mongoose.Types.ObjectId, ref: "Ingredients" }],
        description: { type: String },
        recipe: { type: String },
        image: {
            type: String,
            default:
                "https://res.cloudinary.com/doev6gukc/image/upload/v1620066777/photo_2021-05-03_18-03-30_jzogxs.jpg",
        },
    },
    {
        timestamps: true,
    }
);

const Product = mongoose.model("Products", productsSchema);

module.exports = Product;
