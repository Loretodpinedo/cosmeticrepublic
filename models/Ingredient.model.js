const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const ingredientsSchema = new Schema(
    {
        type: {
            type: String,
            required: true,
            enum: [
                "Aceite",
                "Emulsionante",
                "Aceite esencial",
                "Principio activo",
                "Hidrolato",
                "Arcilla",
                "Tensioactivo",
                "Colorante",
                "Cera",
                "Manteca",
            ],
        },
        name: { type: String, required: true },
        description: { type: String },
    },
    {
        timestamps: true,
    }
);
const Ingredient = mongoose.model("Ingredients", ingredientsSchema);

module.exports = Ingredient;
