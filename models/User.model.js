const mogoose = require("mongoose");

const Schema = mogoose.Schema;

const usersSchema = new Schema(
    {
        name: { type: String, required: true },
        email: { type: String, required: true },
        password: { type: String, required: true },
        role: {
            type: String,
            required: true,
            enum: ["admin", "user"],
            default: "user",
        },
    },
    {
        timestamps: true,
    }
);

const User = mogoose.model("Users", usersSchema);

module.exports = User;
