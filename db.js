const mongoose = require('mongoose');
// const dotenv = require('dotenv');
// dotenv.config();
//const DB_URL = process.env.DB_URL_LOCAL;
const DB_URL = 'mongodb://localhost:27017/cosmetic-republic';

const DB_CONFIG = {
    useNewUrlParser: true,
    useUnifiedTopology: true,

};

const connect = () => {

    mongoose.connect(DB_URL, DB_CONFIG)
        .then((res) => {

            const { name, host} = res.connection;

            console.log(`conectado a ${name} con exito en ${host}`);

        })
        .catch( error => {

            console.log('Error conectando a la BDD', error);

        });

}; 

module.exports = { DB_URL, DB_CONFIG, connect};
            

        
