const indexGet = (req, res) => {
    const pageTitle = "Bienvenides a Un montón de jabón";
    return res.render("index", { title: pageTitle, user: req.user });
};

module.exports = { indexGet };
