const Ingredient = require("../models/Ingredient.model");

const ingredientsGet = async (req, res, next) => {
    try {
        const ingredients = await Ingredient.find();

        return res.render("ingredients/ingredients", {
            ingredients: ingredients,
        });
    } catch (error) {
        console.log("error", error);
        return next(error);
    }
};

const createIngredientGet = (req, res, next) => {
    return res.render("ingredients/create");
};

const ingredientByIdGet = async (req, res, next) => {
    const { id } = req.params;

    try {
        const ingredient = await Ingredient.findById(id);
        return res.render("ingredients/ingredient", { ingredient });
    } catch (error) {
        return next(error);
    }
};

const createIngredientPost = async (req, res, next) => {
    try {
        const { type, name, description } = req.body;
        const newIngredient = new Ingredient({ type, name, description });
        const createdIngredient = await newIngredient.save();

        return res.render("ingredients/ingredient", {
            ingredient: createdIngredient,
        });
    } catch (error) {
        return next(error);
    }
};

const modifyIngredientPut = async (req, res, next) => {
    try {
        const { id, name, description } = req.body;

        const editedIngredient = await Ingredient.findByIdAndUpdate(
            id,
            { name },
            { description },
            { new: true }
        );

        return res.render("ingredients/ingredient", {
            ingredient: editedIngredient,
        });
    } catch (error) {
        return next(error);
    }
};

const deleteIngredientByPost = async (req, res, next) => {
    try {
        const { id } = req.params;

        await Ingredient.findByIdAndDelete(id);

        return res.redirect("/ingredients");
    } catch (error) {
        next(error);
    }
};

module.exports = {
    ingredientsGet,
    createIngredientGet,
    ingredientByIdGet,
    createIngredientPost,
    modifyIngredientPut,
    deleteIngredientByPost,
};
