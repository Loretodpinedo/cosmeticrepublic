const Product = require("../models/Product.model");

const productsGet = async (req, res, next) => {
    try {
        const products = await Product.find().populate("ingredients");
        return res.render("products/products", { products });
    } catch (error) {
        next(error);
    }
};

const productsMenuGet = (req, res) => {
    return res.render("products/products-menu");
};

const productsByType = async (req, res) => {
    const { option } = req.params;
    try {
        const productByType = await Product.find({ type: option }).populate(
            "ingredients"
        );
        console.log(productById);
        return res.render("products/products");
    } catch (error) {
        return next(error);
    }
};

const createProductsGet = (req, res, next) => {
    return res.render("products/create");
};

const productById = async (req, res, next) => {
    const { id } = req.params;

    try {
        const product = await Product.findById(id).populate("ingredients");
        return res.render("products/product", { product });
    } catch (error) {
        return next(error);
    }
};

const createProductsPost = async (req, res, next) => {
    try {
        const { type, name, solid, description, recipe } = req.body;
        const image = req.image_url
            ? req.image_url
            : "https://res.cloudinary.com/doev6gukc/image/upload/v1620066777/photo_2021-05-03_18-03-30_jzogxs.jpg";

        const newProduct = new Product({
            type,
            name,
            solid,
            description,
            recipe,
            image,
        });

        const createdProduct = await newProduct.save();

        return res.render("products/product", { product: createdProduct });
    } catch (error) {
        return next(error);
    }
};

module.exports = {
    productsGet,
    productsMenuGet,
    productsByType,
    createProductsGet,
    productById,
    createProductsPost,
};
