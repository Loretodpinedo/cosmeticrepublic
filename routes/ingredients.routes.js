const express = require("express");
const controller = require("../controllers/ingredients.controller");
const router = express.Router();

router.get("/", controller.ingredientsGet);
router.get("/create", controller.createIngredientGet);
router.get("/:id", controller.ingredientByIdGet);

router.post("/create", controller.createIngredientPost);

router.put("/:id", controller.modifyIngredientPut);

router.post("/:id", controller.deleteIngredientByPost);

module.exports = router;
