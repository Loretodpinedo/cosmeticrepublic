const express = require("express");
const controller = require("../controllers/products.controller");

const router = express.Router();

router.get("/", controller.productsGet);

router.get("/products-menu", controller.productsMenuGet);

router.get("/products/:option", controller.productsByType);

router.get("/create", controller.createProductsGet);

router.get("/:id", controller.productById);

router.post("/create", controller.createProductsPost);

module.exports = router;
