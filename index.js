const express = require("express");
const path = require("path");
const hbs = require("hbs");
const session = require("express-session");
const MongoStore = require("connect-mongo");
const passport = require("passport");
// const dotenv = require("dotenv");
// dotenv.config();

const { isAuthenticated } = require("./middlewares/auth.middleware");


const indexRoutes = require("./routes/index.routes");
const authRoutes = require("./routes/auth.routes");
const ingredientRoutes = require("./routes/ingredients.routes");
const productRoutes = require("./routes/products.routes");
const db = require("./db.js");


require("./authentication");

const PORT =  3000;//process.env.PORT ||

db.connect();

const server = express();

server.use(
    session ({
        // secret: process.env.SESSION_SECRET, 
        secret: 's3cr3t0.s3cr3t0s0Nada!', 
        resave: false,
        saveUninitialized: false,
        cookie: {

            maxAge: 15 * 24 * 60 * 60 * 1000,
        },
        store: MongoStore.create({ mongoUrl: db.DB_URL}),
    })
);

server.use(passport.initialize());
server.use(passport.session());

server.set('views', path.join(__dirname, 'views'));
server.set('view engine', 'hbs');

server.use(express.static(path.join(__dirname, 'public')));

server.use(express.json());

server.use(express.urlencoded({ extended: true }));

server.use("/", indexRoutes);
server.use("/auth", authRoutes);
server.use("/ingredients", ingredientRoutes);
server.use("/products", productRoutes);



server.use("*", (req, res) => {
    const error = new Error ("This way does not exist");
    error.status = 404;
    return res.status(404).json(error.message);
   

});
server.use((error, req, res, next) => {
    console.log("ERROR HANDLER", error);
    const errorStatus = error.status || 500;
    const errorMsg = error.menssage || "Unexpected Error";
    return res.render('error-view', { status: errorStatus, menssage: errorMsg});
});

server.listen(PORT, () => {

    console.log(`Servidor de Un Montón de jabón haciendo pompas a mogollón en http://localhost:${PORT}`);
});
