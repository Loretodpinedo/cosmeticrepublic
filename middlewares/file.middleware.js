const path = require("path");
const fs = require("fs");
const multer = require("multer");
const cloudinary = require("cloudinary").v2;

const ACCEPTED_FILE_EXTENSION = ["image/png", "image/jpg", "image/joeg"];

const storage = multer.diskStorage({
    filename: (req, file, cb) => {
        cb(null, `${Date.now()}-product-${file.originalname}`);
    },
    destination: (req, file, cb) => {
        //cambio esto '../public/uploads' por la dir de cloudinary
        const directory = path.join(
            __dirname,
            "CLOUDINARY_URL=cloudinary://395962839168272:D9pM0FLwSDa6XFcexhaKQwCMeNU@doev6gukc"
        );
        cb(null, directory);
    },
});

const fileFilter = (req, file, cb) => {
    if (ACCEPTED_FILE_EXTENSION.includes(file.mimetype)) {
        cb(null, true);
    } else {
        const error = new Error("This file type dont work here");
        error.status = 400;
        cb(error);
    }
};
const upload = multer({
    storage,
    fileFilter,
});

const uploadToCloudinary = async (req, res, next) => {
    if (req.file) {
        const filePath = req.file.path;
        const imageFromCloudinary = await cloudinary.uploader.upload(filePath);

        req.image_url = imageFromCloudinary.secure_url;

        await fs.unlinkSync(filePath);

        return next();
    } else {
        return next();
    }
};

module.exports = { upload, uploadToCloudinary };
