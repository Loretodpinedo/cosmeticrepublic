const mongoose = require("mongoose");
const Ingredients = require("../models/Ingredient.model");
const { DB_URL, DB_CONFIG } = require("../db");

//["Aceite", "Emulsionante", "Aceite esencial", "Principio activo", "Hidrolato", "Arcilla", "Tensioactivo", "Colorante", "Cera", "Manteca"]

const ingredientsArray = [
    {
        type: "Aceite",
        name: "Aceite monoi de Tahiti",
        description:
            "Tiene las propiedades nutritivas, suavizantes e hidratantes del aceite de coco virgen. Las flores Tiare tienen propiedades calmantes y regeneradoras y un delicioso olor.",
    },

    {
        type: "Emulsionante",
        name: "Cera protelan",
        description: "Amarilla y en escamas. Autoemulsiona que da gusto",
    },

    {
        type: "Aceite esencial",
        name: "Menta Piperita",
        description: "Huele a menta, refrescante",
    },

    {
        type: "Principio activo",
        name: "Bisabobol",
        description: "Suavizante natural, no deja sensación grasa en la piel.",
    },

    {
        type: "Hidrolato",
        name: "Hammamelis",
        description:
            "astringente, calmante, purificante y tonificante. Olor suave, herbáceo",
    },

    {
        type: "Arcilla",
        name: "Arcilla blanca caolin",
        description:
            "Maravilla, maravillosa, sirve para todo y en todo queda bien",
    },

    {
        type: "Tensioactivo",
        name: "SCS",
        description:
            "Favorito de las programadoras porque les recuerda a las hojas de estilos.",
    },

    {
        type: "Colorante",
        name: "Mica verde perlada",
        description: "Preciosa en toda su preciosidad",
    },

    {
        type: "Cera",
        name: "De abejas",
        description: "Huele que alimenta, bastante dureta.",
    },

    {
        type: "Manteca",
        name: "Manteca de Karité",
        description:
            "Sin refinar huele a bacon, sirve para todo, es maravillosa, la amamos sin condiciones.",
    },
];

mongoose
    .connect(DB_URL, DB_CONFIG)
    .then(async () => {
        console.log("Ejecutando seed Ingredients.js");

        const allIngredients = await Ingredients.find();

        if (allIngredients.length) {
            await Ingredients.collection.drop();
            console.log("Colección de ingredientes eliminada exitosamente.");
        }
    })
    .catch((error) => {
        console.log("Error buscando en la BDD:", error);
    })
    .then(async () => {
        await Ingredients.insertMany(ingredientsArray);
        console.log("Los ingredientes han sido añadidos a la BDD");
    })
    .finally(() => {
        mongoose.disconnect();
    });
