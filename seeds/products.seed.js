const mongoose = require("mongoose");
const Products = require("../models/Product.model");
const Ingredient = require("../models/Ingredient.model");
const { DB_URL, DB_CONFIG } = require("../db");

const productsArray = [
    {
        type: "Jabón",
        name: "Jabón jabonoso, dulce y hermoso",
        solid: "true",
        ingredients: [],
        description: "Maravilla de jabón que a todes gusta un montón",
        recipe: "porcentajes e ingredientes, para poner largos los dientes",
        image:
            "https://res.cloudinary.com/doev6gukc/image/upload/v1620066776/photo_2021-05-03_18-03-20_fdl90o.jpg",
    },

    {
        type: "Crema",
        name: "crema de manos",
        solid: "false",
        ingredients: [],
        description:
            "Crema muy nutritiva, que transforma las manos, como la lengua de un gatete, en el pelaje mas suave y brillante.",
        recipe: "75% agua, 5% emulsionante, 20% aceites mágicos",
        image:
            "https://res.cloudinary.com/doev6gukc/image/upload/v1620066776/IMG_20171220_124231_094_nu6x1o.jpg",
    },

    {
        type: "Gel de baño",
        name: "Noches mágicas de invierno",
        solid: "false",
        ingredients: [],
        description:
            "Lleno de burbujas de amor y calor, para las duchas mas frias",
        recipe: "porcentajes e ingredientes, para poner largos los dientes",
        image:
            "https://res.cloudinary.com/doev6gukc/image/upload/v1620066779/IMG_20171220_124312_231_jz8yea.jpg",
    },
    {
        type: "Champú",
        name: "De arcilla verde",
        solid: "true",
        ingredients: [],
        description: "Suave con el pelo, fuerte con la caca",
        recipe: "porcentajes e ingredientes, para poner largos los dientes",
        image:
            "https://res.cloudinary.com/doev6gukc/image/upload/v1620066778/photo_2021-05-03_18-04-32_h58thi.jpg",
    },
    {
        type: "Mascarilla pelo",
        name: "Baratilla para el pelo",
        solid: "true",
        ingredients: [],
        description: "Baratilla para un pelo lustroso como el de un oso",
        recipe: "porcentajes e ingredientes, para poner largos los dientes",
        image:
            "https://res.cloudinary.com/doev6gukc/image/upload/v1620066778/photo_2021-05-03_18-03-53_z4uulp.jpg",
    },
    {
        type: "Dentifrico",
        name: "Piños relucientes, dientes, dientes, dientes",
        solid: "false",
        ingredients: [],
        description: "maravilla, maravillosa de madre osa",
        recipe: "porcentajes e ingredientes, para poner largos los dientes",
        image:
            "https://res.cloudinary.com/doev6gukc/image/upload/v1620066774/IMG_20171104_134756_331_kzscyb.jpg",
    },
];

mongoose
    .connect(DB_URL, DB_CONFIG)
    .then(async () => {
        console.log("Ejecutando seed de Products.js");

        const allProducts = await Products.find();

        console.log(allProducts);
        if (allProducts.length) {
            await Products.collection.drop();
            console.log("Borrados todos los productos de la BDD");
        }
    })
    .catch((error) => {
        console.log("Error buscando en la BDD:", error);
    })
    .then(async () => {
        //console.log('antes de insertar los ingedientes');

        const allIngredients = await Ingredient.find();
        //console.log('todos los ingredientes ', allIngredients);

        productsArray[0].ingredients = [
            allIngredients[0]._id,
            allIngredients[1]._id,
        ];
        productsArray[1].ingredients = [
            allIngredients[1]._id,
            allIngredients[2]._id,
        ];
        productsArray[2].ingredients = [
            allIngredients[2]._id,
            allIngredients[3]._id,
        ];
        productsArray[3].ingredients = [
            allIngredients[5]._id,
            allIngredients[6]._id,
        ];
        productsArray[4].ingredients = [
            allIngredients[7]._id,
            allIngredients[8]._id,
        ];
        //productsArray[5].ingredients = [allIngredients[9]._id, allIngredients[10]._id];

        await Products.insertMany(productsArray);
        console.log("Productos, con sus ingredientes añadidos a BDD");
    })
    .catch((error) => {
        console.log("Algo ha pasado, los productos no se han insertado");
    })
    .finally(() => {
        mongoose.disconnect();
    });
